//
//  AppDelegate.h
//  MyPratice_ios
//
//  Created by HillBao on 2017/8/13.
//  Copyright © 2017年 鲍云峰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

